package com.oreillyauto.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.oreillyauto.service.CarpartsService;

@Controller

public class CarpartsMgrController{
    
    @Autowired
    CarpartsService carpartsService;
    
    @GetMapping(value="/carparts/getcarparts")
    public List<carpart> getCarpartsData() {
        return carpartsService.getCarparts();       
    }   
    
    @GetMapping(value="/carpartsmgr")
    public String getCarpartsmMgr(Model model) {
        return "carpartsmgr";
    }
    
}