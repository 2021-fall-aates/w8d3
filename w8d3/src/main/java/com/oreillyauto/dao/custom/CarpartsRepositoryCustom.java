package com.oreillyauto.dao.custom;

import com.oreillyauto.domain.Carpart;

public interface CarpartsRepositoryCustom {
    public Carpart getCarpartByPartNumber(String partNumber);
}

